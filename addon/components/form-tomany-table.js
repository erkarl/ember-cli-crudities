import Ember from 'ember';
import ToManyBase from './tomany-base';
import layout from '../templates/components/form-tomany-table';

export default ToManyBase.extend({
  layout,
  column_count: Ember.computed('field.fields.length', function() {
    return parseInt(this.get('field.fields.length')) + 1;
  }),
});
