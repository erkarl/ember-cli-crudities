import Ember from 'ember';
import BaseCollection from './base-collection';

const {
  computed,
  isEmpty,
  A
} = Ember;

export default BaseCollection.extend({
  store: Ember.inject.service(),

  softDelete: [],
  deleteCount: 0,

  related: computed('model', 'model.length', 'model.isFulfilled', 'deleteCount', function() {
    const softDelete = this.get('softDelete');
    const value = this.get('model');
    if (!isEmpty(value)) {
      return value.filter(function(item) {
        return softDelete.indexOf(item.get('id')) === -1;
      });
    } else {
      return new A();
    }
  }),

  save() {
    this.get('model').then((model) => {
      const softDelete = this.get('softDelete');
      model.forEach((record) => {
        if (softDelete.indexOf(record.get('id')) === -1) {
          record.save();
        } else {
          record.destroyRecord();
        }
      });
    });
  },

  delete() {
    this.get('model').then((model) => {
      model.forEach((record) => {
        record.destroyRecord();
      });
    });
  },

  actions: {
    addRelated() {
      var related = this.get('model');
      var record = this.get('store').createRecord(related.get('content.type.modelName'), {});
      related.pushObject(record);
    },
    deleteRelated(record) {
      if (record.get('isNew')) {
        record.rollbackAttributes();
      } else {
        this.get('softDelete').push(record.get('id'));
        this.set('deleteCount', this.get('softDelete.length'));
      }
    }
  },

  // didInsertElement() {
  //   Ember.run.later(this, function() {
  //     if (!this.get('isDestroyed')) {
  //       this.rerender();
  //     }
  //   }, 750);
  // },
});
