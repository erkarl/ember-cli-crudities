import Ember from 'ember';
import layout from '../templates/components/form-field';

export default Ember.Component.extend({
  layout,

  tagName: null,
  formLayout: 'horizontal',
  horiClass: 'col-sm-3',
  inputClass: 'col-sm-9',

  isFieldset: Ember.computed.equal('field.widget', 'fieldset'),
  isTabset: Ember.computed.equal('field.widget', 'tabset'),

  isToManyTable: Ember.computed.equal('field.widget', 'tomany-table'),
  isToManySlide: Ember.computed.equal('field.widget', 'tomany-slide'),

  isToMany: Ember.computed.or('isToManyTable', 'isToManySlide'),

  isCollection: Ember.computed.or('isFieldset', 'isTabset', 'isToMany'),

  widgetComponent: Ember.computed('field.widget', function() {
    const widget = this.get('field.widget');
    if (widget) {
      return `form-${widget}`;
    }
    return 'form-input';
  }),

  field_horiClass: Ember.computed.or('{field.horiClass,horiClass}'),
  field_inputClass: Ember.computed.or('{field.inputClass,inputClass}'),
  
  label: Ember.computed.alias('field.label'),
  property: Ember.computed.alias('field.name'),
    
  record: Ember.computed('model', 'model.isFulfilled', 'field.name', function() {
    const model = this.get('model');
    const field_name = this.get('field.name');
    if (field_name !== undefined) {
      const rv = model.get(field_name);
      return rv;
    }
    return model;
  }),
});
