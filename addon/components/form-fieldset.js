import Ember from 'ember';
import BaseCollection from './base-collection';
import layout from '../templates/components/form-fieldset';

export default BaseCollection.extend({
  layout,

  is_ready: false,

  tagName: 'fieldset',
  classNameBindings: ['class', ],
  fields: Ember.A(),
  label: Ember.computed.or('{field.title,field.label}'),
  innerClass: Ember.computed('field.innerClass', function() {
    const innerClass = this.get('field.innerClass');
    if (!Ember.isEmpty(innerClass)) {
      return innerClass;
    }
    return 'col-xs-12';
  }),

  rerenderOnFieldsChange: Ember.observer('fields.[]', function() {
    this.rerender();
  }),

  init() {
    this._super();
    Ember.run.later(this, function() {
      if (!this.get('isDestroyed')) {
        this.set('is_ready', true);
      }
    }, 300);
  }
});
