import Ember from 'ember';

export default Ember.Mixin.create({
  init() {
    this._super();
    Ember.Binding.from('model.' + this.get('property')).to('value').connect(this);
  }
});
