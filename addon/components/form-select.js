import Ember from 'ember';
import layout from '../templates/components/form-select';

export default Ember.Component.extend({
  layout,
  tagName: undefined,
  type: 'select',
});
