import Ember from 'ember';
import BoundValueMixin from '../mixins/boundvalue';
import layout from '../templates/components/form-foreignkey';

export default Ember.Component.extend(BoundValueMixin, {
  layout,
  tagName: undefined,

  store: Ember.inject.service(),
  type: 'foreignkey',
  isExpanded: false,

  selectedIndex: Ember.computed('content', 'content.[]', 'value', {
    get(/* key */) {
      let selectedIndex = -1;
      const value = String(this.get('value.id'));
      const content = this.get('content');
      if (!Ember.isEmpty(content)) {
        content.find(function(item, index) {
          const id = String(item.get('id'));
          if (id === value) {
            selectedIndex = index;
            return true;
          }
          return false;
        });
      }
      return selectedIndex;
    },
    set(key, new_index) {
      new_index = parseInt(new_index);
      this.set('value', this.get('content').objectAt(new_index));
      return new_index;
    }
  }),

  init() {
    this._super();
    const model_name = this.get('extra.related_model');
    this.get('store').findAll(model_name).then((models) => {
      this.set('content', models);
    });
  },

});
