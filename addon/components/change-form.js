import Ember from 'ember';
import layout from '../templates/components/change-form';

export default Ember.Component.extend({
  layout,
  store: Ember.inject.service(),
  modelLoader: Ember.inject.service(),
  inflector: new Ember.Inflector(Ember.Inflector.defaultRules),
  tetherClass: 'modal-dialog modal-lg',
  tetherAttachment: 'middle center',
  tetherTargetAttachment: 'middle center',
  tetherTarget: 'document.body',

  registry: Ember.A(),

  modalOpen: false,
  modalRecord: undefined,
  modalMeta: undefined,
  modalTitle: undefined,
  modelField: undefined,
  isModal: false,

  defaults: {},
  fieldsets: [{
    title: null,
    fields: [
      {
        name: '__str__',
        label: 'Object',
        read_only: true,
        widget: 'input',
      }
    ]
  }],
  fetch_meta: true,

  model: Ember.computed('id', 'model_name', function() {
    const id = this.get('id');
    if (id !== undefined) {
      return this.get('store').findRecord(this.get('model_name'), id);
    }
    return this.get('store').createRecord(
      this.get('model_name'),
      this.get('defaults')
    );
  }).volatile(),

  register(/* collection */) {
    // console.log('registering', collection);
    // const registry = this.get('registry');
    // const foundAt = registry.indexOf(collection);
    // if (foundAt === -1) {
    //   registry.pushObject(collection);
    // }
    // console.log(this.get('registry'));
  },

  unregister(/* collection */) {
    // console.log('UNregistering', collection);
    // const registry = this.get('registry');
    // this.set('registry', registry.filter((item) => {
    //   return item !== collection;
    // }));
    // console.log(this.get('registry'));
  },

  _do_get_model(value) {
    if (value.hasOwnProperty('isFulfilled')) {
      return value.get('content');
    }
    return value;
  },

  _do_for_all(record, fieldsets, method) {
    const top_promises = [];
    const models = [];
    /** TODO: Can probably be improved with ember-concurency */
    return new Ember.RSVP.Promise((resolve_top, reject_top) => {
      let promise;
      if (method === 'rollbackAttributes') {
        promise = new Ember.RSVP.Promise((resolve) => {
          record[method]();
          resolve(record);
        });
      } else {
        promise = new Ember.RSVP.Promise((resolve, reject) => {
          record[method]().then(resolve).catch(reject);
        });
      }
      promise.then((saved_record) => {
        models.push(saved_record);
        fieldsets.forEach((fieldset) => {
          if (fieldset.fields !== undefined) {
            fieldset.fields.forEach((field) => {
              if (field.widget.split('-')[0] === 'tomany') {
                top_promises.push(new Ember.RSVP.Promise((resolve, reject) => {
                  const promises = [];
                  record.get(field.name).then((related) => {
                    related.forEach((related_record) => {
                      if (models.indexOf(related_record) === -1) {
                        promises.push(
                          this._do_for_all(related_record, [field.fields], method).then((saved_related) => {
                            models.push(saved_related);
                          })
                        );
                      }
                    });
                    Ember.RSVP.Promise.all(promises)
                      .then(resolve)
                      .catch((err) => { reject(err); });
                  });
                }));
              }
            });
          }
        });
        Ember.RSVP.Promise.all(top_promises)
          .then(() => {
            resolve_top(record);
          })
          .catch((err) => {
            reject_top(err);
          });
      });
    });
  },

  after_save(/* model */) {
    /* hook */
  },

  do_delete(/* model, fieldsets */) {
    /* FIXME: not implemented yet */
  },

  actions: {

    add_related(model_name, field) {
      const record = this.get('store').createRecord(model_name, {});
      this.set('modalRecord', record);
      this.set('modalField', field);
      const inflector = this.get('inflector');
      let parts = model_name.split('/');
      this.set('modalTitle', `Add ${parts[1]}`);
      parts[1] = inflector.pluralize(parts[1]);
      const modelLoader = this.get('modelLoader');
      modelLoader.get_model_meta(parts[0], parts[1]).then((meta) => {
        this.set('modalMeta', meta);
        this.set('modalOpen', true);
      });
    },

    edit_related(model_name, field) {
      const record = field.get('value');
      this.set('modalRecord', record);
      this.set('modalField', field);
      const inflector = this.get('inflector');
      let parts = model_name.split('/');
      this.set('modalTitle', `Edit ${parts[1]} ${record.get('__str__')}`);
      parts[1] = inflector.pluralize(parts[1]);
      const modelLoader = this.get('modelLoader');
      modelLoader.get_model_meta(parts[0], parts[1]).then((meta) => {
        this.set('modalMeta', meta);
        this.set('modalOpen', true);
      });
    },

    cancel_related() {
      const model = this._do_get_model(this.get('modalRecord'));
      const fieldsets = this.get('modalMeta.fieldsets');
      this._do_for_all(model, fieldsets, 'rollbackAttributes').then(() => {
        this.set('modalOpen', false);
        this.set('modalMeta', undefined);
        this.set('modalTitle', undefined);
        this.set('modelField', undefined);
      }).catch((err) => {
        console.error(err);
      });
    },

    save_related() {
      const model = this._do_get_model(this.get('modalRecord'));
      const field = this.get('modalField');
      const fieldsets = this.get('modalMeta.fieldsets');
      this._do_for_all(model, fieldsets, 'save').then(() => {
        field.set('value', model);
        this.set('modalOpen', false);
        this.set('modalMeta', undefined);
        this.set('modalTitle', undefined);
        this.set('modelField', undefined);
      }).catch((err) => {
        console.error(err);
      });
    },

    save_all() {
      this._do_for_all(this.get('model'), this.get('fieldsets'), 'save').then((model) => {
        this.after_save(model);
      });
    },

    delete_all() {
      this.do_delete(this.get('model'), this.get('fieldsets'));
    },
  }

});
