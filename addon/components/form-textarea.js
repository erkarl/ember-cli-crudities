import BaseWidget from './base-widget';

export default BaseWidget.extend({
  type: 'textarea',
});
