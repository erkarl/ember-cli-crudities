import Ember from 'ember';

export default Ember.Component.extend({
  modelLoader: Ember.inject.service(),

  fetch_meta: true,

  fieldsChanged: function() {
    const rv = Ember.A();
    let innerHoriClass = this.get('field.innerHoriClass');
    if (Ember.isEmpty(innerHoriClass)) {
      innerHoriClass = 'col-sm-3';
    }
    let innerInputClass = this.get('field.innerInputClass');
    if (Ember.isEmpty(innerInputClass)) {
      innerInputClass = 'col-sm-9';
    }
    const fields = this.get('field.fields');
    if (!Ember.isEmpty(fields)) {
      fields.forEach((field) => {
        if (Ember.isEmpty(field.horiClass)) {
          field.horiClass = innerHoriClass;
        }
        if (Ember.isEmpty(field.inputClass)) {
          field.inputClass = innerInputClass;
        }

        if (!Ember.isEmpty(field.target_model) && field.targetModel === undefined) {
          const target_model = this.get('store').findAll(field.model);
          field.targetModel = target_model;
        }
        rv.pushObject(field);
      });
    }
    this.set('fields', rv);
  },

  unregister_with_main() {
    const main = this.get('main');
    if (main !== undefined) {
      main.unregister(this);
    }
  },

  register_with_main() {
    const main = this.get('main');
    if (main !== undefined) {
      main.register(this);
    }
  },

  save() {
    const do_save = this.get('field.extra.save');
    if (do_save === true) {
      this.get('model').then((model) => {
        model.save();
      });
    }
  },

  delete() {
    const do_save = this.get('field.extra.save');
    if (do_save === true) {
      this.get('model').then((model) => {
        model.destroyRecord();
      });
    }
  },

  init() {
    this._super();
    this.fieldsChanged();
    if (this.get('fetch_meta') === true) {
      let fieldset = this.get('field');
      const modelLoader = this.get('modelLoader');
      const inflector = new Ember.Inflector(Ember.Inflector.defaultRules);

      const model = this.get('model');
      let model_name = model.get('content._internalModel.type.modelName');
      if (model_name === undefined) {
        model_name = model.get('content.type.modelName');
      }
      if (model_name === undefined) {
        model_name = model.get('_internalModel.type.modelName');
      }
      if (model_name === undefined) {
        model_name = model.get('type.modelName');
      }
      let plural_name = model_name.split('/');
      plural_name[1] = inflector.pluralize(plural_name[1]);

      modelLoader.get_model_meta(plural_name[0], plural_name[1]).then((meta) => {
        const merged = modelLoader.merge_fields_fieldset(meta.fields, fieldset);
        Ember.set(fieldset, 'fields', merged.fields);
        if (!(this.get('isDestroyed')) && !Ember.isEmpty(fieldset.fields)) {
          this.set('field', fieldset);
          this.set('fetch_meta', false);
          this.fieldsChanged();
        }
      });
    }
    this.register_with_main();
  },

  willDestroyElement() {
    this.unregister_with_main();
  },

});
