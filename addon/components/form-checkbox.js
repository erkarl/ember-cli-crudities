import FormInput from './form-input';

export default FormInput.extend({
  type: 'checkbox'
});
